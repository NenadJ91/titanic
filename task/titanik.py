import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    def person(x):
        if 'Mr.' in x:
            return 1
        if 'Mrs.' in x:
            return 2
        if 'Miss.' in x:
            return 3
    
    df['nova_kolona'] = df['Name'].apply(lambda x: person(x))
    group_by_prefix = df.groupby('nova_kolona')

    y,m,n = group_by_prefix['Age'].median().round(2).values
    x = df['Age'][df['nova_kolona']==1].isnull().sum()
    k = df['Age'][df['nova_kolona']==2].isnull().sum()
    l = df['Age'][df['nova_kolona']==3].isnull().sum()

    return [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    
